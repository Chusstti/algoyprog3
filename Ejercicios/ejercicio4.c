#include <stdio.h>

int main() {
    int num[] = {1,2,3,5,9,6};
    int sum = 0;
    for (int i = 1; i < sizeof(num) / sizeof(int); i++) {
        sum = sum + num[i];
    }
    float prom = (float) sum / sizeof(num) *sizeof(int);
    printf("Promedio: %f", prom);
    return 0;
}