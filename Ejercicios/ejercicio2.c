#include <stdio.h>

int main() {
    int num[] = {1,2,3,50,79,6,100,-1};
    int max = num[0];
    for (int i = 1; i < sizeof(num) / sizeof(int); i++) {
        if (num[i] > max) {
        max = num[i];
        }
    }
    printf("Máximo: %d", max);
    return 0;
}