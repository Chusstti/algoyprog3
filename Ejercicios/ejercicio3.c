#include <stdio.h>

int main() {
    int num[] = {1,2,3,50,79,6,100,-1};
    int min = num[0];
    for (int i = 1; i < sizeof(num) / sizeof(int); i++) {
        if (num[i] < min) {
        min = num[i];
        }
    }
    printf("Min: %d", min);
    return 0;
}